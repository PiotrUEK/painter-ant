#pragma once

#include <stddef.h>
#include <stdbool.h>

typedef void (*actionProc)();   // Prototype of action procedure

// Automaton rule description
typedef struct {
    int state;              // Current automaton state
    int input;              // Received input signal
    actionProc action;      // Action to be performed (or NULL)
    int newState;           // New state to be assumed
} RULE;

// Automaton description
typedef struct {
    RULE* rules;            // Pointer to array of rules
    size_t rulesNum;        // Number of entries in rules array
    int currentState;       // Current state (initial state at the beginning)
} AUTOMATON;

// Rule wildcards
enum {
    ANY_STATE = -1,         // Matches any state
    ANY_SIGNAL = -1,        // Matches any input signal
    SAME_STATE = -2,        // Leaves current state unchanged
    NO_SIGNAL = -2          // Dummy signal for placeholder rules
};

bool processInput(AUTOMATON* fsm, int input);
